(define-module (deprecated gnu packages postgresql)
  #:use-module (gnu packages databases)
  #:use-module (guix download)
  #:use-module (guix packages))

(define-public postgresql-deprecated
(package
 (inherit postgresql)
 (name "postgresql-deprecated")
 (version "9.1.24")
 (source (origin
	  (method url-fetch)
	  (uri (string-append "https://ftp.postgresql.org/pub/source/v"
			      version "/postgresql-" version ".tar.bz2"))
	  (sha256
	   (base32
	    "1lz5ibvgz6cxprxlnd7a8iwv387idr7k53bdsvy4bw9ayglq83fy"))))))
 
